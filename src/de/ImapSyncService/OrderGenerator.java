//--------------------------------------------------------------------------------------------------------------------
//ImapSyncService
//
//The MIT License (MIT)
//
//Copyright (c) 2015 Sascha Deeg
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation the 
//rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//--------------------------------------------------------------------------------------------------------------------
package de.ImapSyncService;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class OrderGenerator {
	
	public void makeOrder(String[] args) throws NumberFormatException, IOException, JAXBException
	{
		SyncOrder order = new SyncOrder();
		order.OrderName = args[1];
		order.SourceServer = args[2];
		order.SourcePort = Integer.parseInt(args[3], 10);
		order.SourceUser = args[4];
		order.SourcePassword = args[5];
		
		order.TargetServer = args[6];
		order.TargetPort = Integer.parseInt(args[7], 10);
		order.TargetUser = args[8];
		order.TargetPassword = args[9];
		
		
        FileOutputStream fileOut = new FileOutputStream(order.OrderName+".syncOrder");
        JAXBContext jaxbContext = JAXBContext.newInstance(SyncOrder.class);
        Marshaller m = jaxbContext.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
        m.marshal(order, fileOut);
        fileOut.flush();
        fileOut.close();
        System.out.printf("Generate Order to:"+order.OrderName+".syncOrder");
		
	
	}

}
